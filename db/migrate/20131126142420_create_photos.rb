class CreatePhotos < ActiveRecord::Migration
     def change
          create_table :photos do |t|
            t.integer :user_id
            t.string :image_file_name
            t.string :image_content_type
            t.string :image_file_size
            t.string :image_update_at
            t.string :content
            t.string :image_url

            t.timestamps
    end
    add_index :photos, [:user_id, :created_at]
end
def self.up 
    add_column :photos, :image_file_name, :string 
    add_column :photos, :image_content_type, :string 
    add_column :photos, :image_file_size, :string 
    add_column :photos, :image_update_at, :string 
end 

def self.down 
    remove_column :photos, :image_file_name, :string 
    remove_column :photos, :image_content_type, :string 
    remove_column :photos, :image_file_size, :string 
    remove_column :photos, :image_update_at, :string 
end

end