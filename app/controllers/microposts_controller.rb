class MicropostsController < ApplicationController
  before_action :signed_in_user, only: [:create, :destroy]
  before_action :correct_user,   only: :destroy
  
  

  def create
    @micropost = current_user.microposts.build(micropost_params)
    @photo = @micropost.build_photo
    if @micropost.save
      flash[:success] = "Micropost created!"
      redirect_to root_url
    else
      @feed_items = []
      render 'static_pages/home'
    end
  end

  def destroy
    @micropost.destroy
    redirect_to root_url
  end
  
  private

  def micropost_params
    params.require(:micropost).permit(:content, photo_attributes: [image:  
                [:image_file_name, :image_content_type, :image_file_size, :image_update_at]])
  end

  def correct_user
    @micropost = current_user.microposts.find_by(id: params[:id])
    redirect_to root_url if @micropost.nil?
  end
end