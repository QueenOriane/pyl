require "open-uri"

class Photo < ActiveRecord::Base
  belongs_to :user
  has_attached_file :image,
  :styles => { :medium => "x300", :thumb => "x100" },
  :default_url => "www.google.fr",
  :storage => :s3,
  :s3_host_name => 's3-eu-west-1.amazonaws.com',
  :bucket => 'pyloriruc',
  :s3_credentials => S3_CREDENTIALS
    
 private
def image_url_provided?
!self.image_url.blank?
end
def download_remote_image
io = open(URI.parse(image_url))
self.original_filename = io.base_uri.path.split('/').last
self.image = io
self.image_remote_url = image_url
rescue # catch url errors with validations instead of exceptions (Errno::ENOENT, OpenURI::HTTPError, etc...)
end
end
